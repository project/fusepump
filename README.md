INTRODUCTION
------------
FusePump BuyNow shortens and simplifies customer journeys from any
digital channel.So, rather than just hoping that customers remember
your marketing efforts, BuyNow can channel engaged customers 
directly to purchasing options.

FusePump Features-

- Turn your brand website into a shoppable site
- Where To Buy store locator shows offline purchase options
- Promote offers in all off-domain channels, including social media
- Use simple tools for custom lightbox design Create trackable links
  Analyse and optimise marketing ROI


INSTALLATION
------------

1) Download the module and place it with other contributed modules
(e.g. modules/contrib).

2) Enable the FusePump module on the Modules list page.

3) Go to any content type and add the FusePump Widget field.

4) Go to that Node type page under creating a node form.

5) Fill the required detail and save the Node. The landing page should reflect
FusePump Integration.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.

Set up FusePump
--------------------

Please visit Fusepump official site https://www.fusepump.com for details.

Here we just need fusepump id to use this connector.

Extend
-------
 - Hook_theme_hook_alter for extending default template of FusePump.
 - Override default css libraries to change default UI/UX.

The module comes with simple twig template called "fusepump-widget.html.twig"
you can copy this template in your theme and customize it.
The more important thing to take in account is the way in 
which the HTML Tag is rendered into this template

{{ html_tag | raw }}

Don't forget the use of the "raw" filter to render properly
the tag, otherwise the HTML Tags as plain text will be output.
