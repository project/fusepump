<?php

namespace Drupal\fusepump\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'fusepump' widget.
 *
 * @FieldWidget(
 *   id = "fusepump_widget",
 *   module = "fusepump",
 *   label = @Translation("FusePump"),
 *   field_types = {
 *     "field_fusepump"
 *   }
 * )
 */
class FusePumpWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];
    $form_element['id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fusepump ID'),
      '#default_value' => $items[$delta]->id,
      '#size' => 50,
      '#maxlength' => 255,
    ];
    $form_element['fallback'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fallback'),
      '#default_value' => $items[$delta]->fallback,
      '#size' => 50,
      '#maxlength' => 255,
    ];
    $element += $form_element;
    return $element;
  }

}
