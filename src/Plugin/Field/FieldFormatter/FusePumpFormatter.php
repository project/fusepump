<?php

namespace Drupal\fusepump\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'fusepump' formatter.
 *
 * @FieldFormatter(
 *   id = "fusepump_formatter",
 *   module = "fusepump",
 *   label = @Translation("FusePump formatter"),
 *   field_types = {
 *     "field_fusepump"
 *   }
 * )
 */
class FusePumpFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#theme' => 'fusepump_widget',
        '#id' => $item->id,
        '#fallback' => $item->fallback,
        '#field_id' => $item->getEntity()->id(),
        '#delta' => $delta,
        '#attached' => [
          'library' => 'fusepump/fusepump',
        ],
      ];
    }

    return $element;
  }

}
