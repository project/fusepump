<?php

namespace Drupal\fusepump\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'fusepump' field type.
 *
 * @FieldType(
 *   id = "field_fusepump",
 *   label = @Translation("FusePump"),
 *   module = "fusepump",
 *   description = @Translation("FusePump"),
 *   category = @Translation("General"),
 *   default_widget = "fusepump_widget",
 *   default_formatter = "fusepump_formatter"
 * )
 */
class FusePump extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'id' => [
          'description' => 'The id of the product.',
          'type' => 'varchar',
          'length' => 2048,
        ],
        'fallback' => [
          'description' => 'Fallback URL',
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['id'] = DataDefinition::create('string')
      ->setLabel(t('ID'));

    $properties['fallback'] = DataDefinition::create('string')
      ->setLabel(t('Fallback'));

    return $properties;
  }

}
