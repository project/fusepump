/**
 * @file
 * Contains the definition of the behaviour jsTestRedWeight.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  /**
   * Attaches the JS test behavior to weight div.
   */
  Drupal.behaviors.jsTestRedWeight = {
    attach: function (context, settings) {
      $('.fusepump-link').click(function(event) {
        event.preventDefault();
        var id = $(this).attr('data-id'); 
        var app = new fusepump.lightbox.buynow(id);
        app.show();
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
